`r if (knitr::is_latex_output()) '<!--'`
# Cryptocurrencies
`r if (knitr::is_latex_output()) '-->'`

```{r, include=FALSE}
library(DiagrammeR)
source("common.R")
```

\chapter{Cryptocurrencies}
\label{chap:literature}

```{r, include=FALSE}
library(caviarma)
library(plotly)
library(openssl)
library(microbenchmark)

get_normalized_prices <- function(ticker = "GC=F", first_date = "2020-01-01") {
  # Get data from Yahoo! finance
  ohlc_prices <- get_price_hist(ticker, date_from = first_date, crypto = F)$ohlc

  # Divide by first price
  normalized_prices <- as.data.frame(
    sapply(ohlc_prices, function(y) as.numeric(y) / head(as.numeric(y), 1)),
    row.names = rownames(ohlc_prices)
  )
}

gold_price <- get_normalized_prices("GC=F")
silver_price <- get_normalized_prices("SI=F")
btc_price <- get_normalized_prices("BTC-CAD")
vix_price <- get_normalized_prices("^VIX")
dollar_price <- get_normalized_prices("CAD=X")
jpy_price <- get_normalized_prices("JPYCAD=X")
```

Societies dating back to the ancient Greeks archaic period (750 – 500 BC) have used metals such as silver and gold [@migeotte-2009] to build coins, which are then used either to store or to exchange value. ^[There are also several industrial applications for these metals.] Gold and silver are not money by nature, but money's nature requires something like gold and silver [@marx-1890] because of their intrinsic value as rare metals (their *Substatzwert*) as well as for their ability to be mint and other potential industrial uses (their *Gebrauchswert*). ^[Storing value in bullions vs. coins mint for day-to-day transactions.] Gold is considered a safe-haven asset in modern financial markets [@gold-2010] since it is expected to keep or increase its value in volatile markets. Figure \@ref(fig:gold-silver-btc) shows the normalized prices ^[Each price series starts from  1.] of the following assets since `r head(rownames(gold_price), 1)`:

+ Gold `GC=F` (USD).
+ Silver `SI=F` (USD).
+ CBOE Volatility Index `^VIX` (USD).
+ USD `CAD=X` (CAD).
+ JPY `JPYCAD=X` (CAD).
+ Bitcoin `BTC-CAD` (CAD).

In this chapter we introduce the main building piece behind Bitcoin's success: the *blockchain* and its proof-of-work consensus mechanism which represents the computer *labour* required to produce a *Bitcoin*, its digital *Substanzwert*. The usefulness of a Bitcoin from a trading perspective, its *Gebrauchswert*, depends exclusively on its acceptance as a payment method in exchange for digital and physical goods and services. This acceptance has been growing since Bitcoin's inception as evidenced by:

+ its growing popularity in countries facing rapid inflation and currency restrictions (@bitcoin-argentina and @argentina-2021).
+ its growing acceptance as a means of payment in electronic transactions (@bitcoin-tesla, @bitcoin-paypal and @bitcoin-paypal-2).

Cryptocurrencies constitute a largely unregulated asset class aiming to compete with traditional currencies and metals such as gold and silver as both a digital reserve currency and a safe-haven asset. Because of this, the most challenging risk faced by cryptocurrencies such as Bitcoin come from regulation, which could potentially render illegal holding or trading such crypto-assets. There exists evidence of such behaviour from world powers dating back to the Greek archaic period [@migeotte-2009] whose political and economic power is linked to the use of their currency to settle transactions and to collect taxes. On the other hand, growing investors' concerns about the environmental impact of cryptocurrency mining hardware are opening the door for new and more sustainable blockchain-based cryptocurrencies.

```{r gold-silver-btc, dev="pdf", fig.cap="Gold, Silver (futures) and Bitcoin (spot) prices vs. VIX.", warning=FALSE, echo=FALSE}
y_max <- max(gold_price, silver_price, vix_price, dollar_price, jpy_price)

last_crisis = list(
  list(type = "rect",
    fillcolor = "blue", line = list(color = "blue"), opacity = 0.3,
    x0 = as.POSIXct("2020-03-01"), x1 = as.POSIXct("2020-06-01"), xref = "x",
    y0 = 0, y1 = y_max, yref = "y"
  )
)

ohlc_chart <- gold_price %>%
  plotly::plot_ly(
    x = ~as.POSIXct(rownames(gold_price)),
    type = "scatter", mode = "lines",
    y = ~CLOSE,
    name = "Gold Futures") %>%
  plotly::add_trace(
    data = silver_price,
    x = ~as.POSIXct(rownames(silver_price)),
    type = "scatter", mode = "lines",
    y = ~CLOSE,
    name = "Silver futures") %>%
  plotly::add_trace(
    data = dollar_price,
    x = ~as.POSIXct(rownames(dollar_price)),
    type = "scatter", mode = "lines",
    y = ~CLOSE,
    name = "USD to CAD") %>%
  plotly::add_trace(
    data = jpy_price,
    x = ~as.POSIXct(rownames(jpy_price)),
    type = "scatter", mode = "lines",
    y = ~CLOSE,
    name = "JPY to CAD") %>%
  plotly::add_trace(
    data = btc_price,
    x = ~as.POSIXct(rownames(btc_price)),
    type = "scatter", mode = "lines",
    y = ~CLOSE,
    name = "BTC to CAD") %>%
  plotly::add_trace(
    data = vix_price,
    x = ~as.POSIXct(rownames(vix_price)), type = "bar", color = I("lightgray"),
    y = ~CLOSE, name = "VIX Index") %>%
  layout(xaxis=list(title="", rangeslider=list(visible=FALSE)),
         yaxis = list(title = "Price"), shapes = last_crisis,
         title = "Normalized prices starting  at 1", legend = list(orientation="h"))
ohlc_chart
```

`r if (knitr::is_latex_output()) '# Bitcoin' else '## Bitcoin'`

The Bitcoin cryptocurrency was introduced by @nakamoto-2008 as a mechanism to make electronic payments between two parties without the need of a trusted third party. A transaction between two parties using Bitcoin takes place in the same way as a typical physical transaction involving cash, i.e., a good or service is exchanged for a predetermined amount of coins and the transaction is non-reversible since the parties identities are never required: the value is stored in the coins. Since no intermediary is involved in a cryptocurrency transaction, there is no central authority capable of modifying past records.

The need to reverse transactions arise from the inherent uncertainty of doing business over an electronic communications channel since one of the parties can easily default on its obligations, e.g., the delivery of a good or service. However, transaction costs increase with the need of a dispute mediation mechanism and non-reversible electronic transactions are not possible since a third party can potentially mediate disputes. Physical transactions using cash require the buyer and seller to be present for the transaction to take place and no third party is involved: cash transactions are also non-reversible. ^[A cash transaction does not require the identities to be disclosed.]

The goal of the Bitcoin technology is to serve as cash for digital transactions, hence avoiding the need of a third party while accepting the same condition one accepts when using cash, i.e., that the exchange is non-reversible. When using a traditional coin or a Bitcoin, the coin itself serves two purposes: *storing* and *exchanging* value. To attain this goal, a new privacy model is proposed by @nakamoto-2008 as shown in Figure \@ref(fig:privacy-model). This new model requires all Bitcoin transactions to be public, which means that anybody can known its time and amount. However, the transactions are not linked to any identities. This level of privacy is comparable to a level II quote from a stock exchange, where volume and prices in the book are made public, but not the identities of the investors.


```{r privacy-model, dev = "pdf", echo = FALSE, fig.cap="Traditional vs. new privacy model."}
grViz('
  digraph dot {
    graph [layout = dot, rankdir = LR]

    subgraph cluster1 {
      label = "Bitcoin Privacy Model";
      labeljust=l;
      bgcolor="white";

      trans1 [
        label="Transactions",
        shape=box
      ];
      public1 [
        label="Public",
        shape=box
      ];

      subgraph cluster11 {
        bgcolor="gray";
        label = "";

        node [
          style=filled,
          shape=box
        ];
        ids1 [
          label="Identities"
        ];
      }

      ids1 -> trans1  [style="filled,setlinewidth(0)", arrowhead=none];
      trans1 -> public1 [style="filled,setlinewidth(1)"];
    }

    subgraph cluster0 {
      label = "Traditional Privacy Model";
      labeljust=l;
      bgcolor="white";

      public [
        label="Public",
        shape=box
      ];

      subgraph cluster00 {
        label = "";
        bgcolor="gray";

        node [
          style=filled,
          shape=box,
          bgcolor = gray
        ];
        ids [
          label="Identities"
        ];
        trans [
          label="Transactions"
        ];
        third_party [
          label="Trusted \nThird Party"
        ];
        counterparty [
          label="Counterparty"
        ];

        ids -> trans [style="filled,setlinewidth(1)"];
        trans -> third_party [style="filled,setlinewidth(1)"];
        third_party -> counterparty [style="filled,setlinewidth(1)"];
      }

      counterparty -> public [style="filled,setlinewidth(0)", arrowhead=none];

    }

  }
')
```

`r if (knitr::is_latex_output()) '# Blockchain' else '## Blockchain' `

A crucial piece of technology behind Bitcoin is its public distributed ledger: the *Blockchain*. Bitcoin transactions data is first timestamped and digitally signed, then written into a *block*. This blocks of transaction records are linked to each other through a unique *Hash* obtained using SHA-256 and creating a chronologically ordered chain of transaction records or blockchain. This public ledger includes the history of all transactions since the beginning of (Bitcoin) time and is equivalent to a bank's private ledger, where a bank would keep track of currency ownership. Now, imagine that the banks ledger itself, let's say an Excel spreadsheet, is the currency itself. In that hypothetical case, the Excel file would be equivalent to the *blockchain*: a database keeping track of money ownership and movements. Also, if one were to create such an Excel-based blockchain, the file sheets or tabs representing the individual blocks should be chronologically ordered, with several transactions per sheet as well as the corresponding cryptographic [nonce](https://en.wikipedia.org/wiki/Cryptographic_nonce) (*number once*), previous hash and current hash as shown in Figure \@ref(fig:bitcoin-block) as per the current Bitcoin [specification](https://developer.bitcoin.org/reference/block_chain.html). The source code is available at `https://github.com/bitcoin/bitcoin/blob/master/src/primitives/block.h`.

Each transaction is timestamped and digitally signed by the two parties and once a block has been created a process called *Mining* begins during which the hash must be computed taking as input the data from the block. The input data is hashed using a Merkle Tree [@merkle-1980] and only the root of the tree is used when computing the block hash to save disk space.

The blockchain serves hence two purposes: it is the technology where transactions take place and it is its own database. Creating a new block, a process called *Mining*, requires finding a hash of all the data contained in the block while satisfying additional constraints. This mining step can take minutes to hours depending on the size of the block and the central processing unit (CPU) power available to find the hash. This computing *work* requires electricity and CPU to find a hash, which is then fed into the next block. Modifying the original data in any previous block requires redoing the work to find a new hash for that block and for any future blocks. Network nodes will by design always consider the longest chain to be the *true* chain and will keep working to find future hashes, which means that the Bitcoin system is designed to always follow the chain requiring the greatest computing power to be produced.

```{r bitcoin-block, dev = "pdf", echo = FALSE, fig.cap="A block and a chain of blocks using Bitcoin's data types."}
grViz('
  digraph dot {
    graph [layout = dot, rankdir = LR]
    compound=true;
    splines=false;

    subgraph cluster0 {
      label = "Block t";
      bgcolor="white";

      subgraph cluster00 {
        label = "";
        bgcolor="gray";

        node [
          style=filled,
          shape=box,
          bgcolor = gray
        ];
        prev_hash [
          label="Previous Hash",
          color = white
        ];
        nonce [
          label="Nonce"
        ];
        tx [
          label="Block Version"
        ];
        txx [
          label="Merkle Root"
        ];
        txxx [
          label="nBits"
        ];
        block_time [
          label="Unix Time"
        ];

        prev_hash->nonce->block_time [arrowhead=none, style="filled,setlinewidth(0)"];
        tx -> txx -> txxx [arrowhead=none, style="filled,setlinewidth(0)"];
      }

      node [
        shape=rectangle,
        bgcolor = white
      ];
      block_hash [
        label="Block Hash"
      ];

      txx -> {block_hash} [arrowhead=none, ltail=cluster22,  style="filled,setlinewidth(0)"];
    }

    subgraph cluster1 {
      label = "Block t + 1";
      bgcolor="white";

      subgraph cluster11 {
        label = "";
        bgcolor="gray";

        node [
          style=filled,
          shape=box,
          bgcolor = gray
        ];
        prev_hash1 [
          label="Previous Hash\n(uint256)",
          color = white
        ];
        nonce1 [
          label="Nonce\n(int32_t)"
        ];
        tx1 [
          label="Block Version\n(int32_t)"
        ];
        txx1 [
          label="Merkle Root\n(uint256)"
        ];
        txxx1 [
          label="nBits\n(int32_t)"
        ];
        block_time1 [
          label="Unix Time\n(int32_t)"
        ];

        prev_hash1->nonce1->block_time1 [arrowhead=none, style="filled,setlinewidth(0)"];
        tx1 -> txx1 -> txxx1 [arrowhead=none, style="filled,setlinewidth(0)"];
      }

      node [
        shape=rectangle,
        bgcolor = white
      ];
      block_hash1 [
        label="Block Hash\n(uint256)"
      ];

      txx1 -> {block_hash1} [arrowhead=none, ltail=cluster22,  style="filled,setlinewidth(0)"];
    }

    block_hash -> {prev_hash1} [ltail=cluster0, lhead=cluster1, style="filled,setlinewidth(1)", minlen=3];

  }
')
```

`r if (knitr::is_latex_output()) '# Proof of work' else  '## Proof of work'`

From a security perspective, the system is vulnerable by design to an attacker capable of controlling more than half of the available CPU in the network since the attacker could build a parallel longer chain that would eventually be followed by the honest nodes as shown in Figure \@ref(fig:attacker-prob-plot). The underlying logic to tolerate this risk is explained by @nakamoto-2008 as follows: someone capable of controlling more than half of the network's computing resources must decide whether to use their resources to maintain or to destroy their own wealth. It is important to mention that other cryptocurrencies and blockchain technologies use alternatives to the proof-of-work consensus algorithm such as Ethereum's proof-of-stake system [@buterin-2013], where the weight of a node is proportional to its currency holdings instead of its computing power.

The proof-of-work system works by restricting the hash of the block to a fixed number of leading zeros which requires some time to be found since it must be randomly guessed. The time that it takes to find a valid block hash depends on the block's difficulty, which is controlled through the `nBits` target number. The SHA-256 hash of an empty string `""` obtained with the `openssl` package [@R-openssl] is:

```{r}
openssl::sha256(paste0(data="", block="", nonce=""))
```

which would not be a valid Bitcoin block hash since its leading characters are not zeros. However, the hash corresponding to `"1134816"` is

```{r}
openssl::sha256(paste0(data="", block="1", nonce="134816"))
```
Which has 5 leading zeros. A current [Bitcoin's](https://www.blockchain.com/btc/blocks?page=1) hash needs 19 leading zeros to be considered valid and accepted by the network. The average [median confirmation time](https://www.blockchain.com/charts/median-confirmation-time) from `2020-04-06` to `2021-04-04` was `12.01` minutes.

Since it takes *time* or computer *work* to find a valid block hash, the longer chain is the one which needed the most CPU power to be produced, i.e., to find valid block hashes. The R loop below shows a simple example of the type of work necessary to find a block hash:

```{r, eval=FALSE}
difficulty <- 5
max_nonce <- 1e6
for(i in 1:max_nonce) {
  hash_i <- openssl::sha256(paste0(block_data="", block_number=1, nonce=i))
  if(substr(hash_i, 1, difficulty) == strrep(0, difficulty)) {
    break
  }
}
hash_i
```

Since every block includes the hash of the previous block as shown in Figure \@ref(fig:bitcoin-block), modifying any data in a past block of the chain will render the remaining blocks invalid and a new mining process will be required to fix all the hashes that have been invalidated by the data tampering. This means that unless an attacker is capable of producing a longer chain faster than all honest nodes in the system by controlling more than half of the system's computing power, the network is capable of ensuring its own data integrity.

To link two blocks, we need to add an additional `prev_block_hash` data field to our previous example:

```{r, eval=FALSE}
a_block <- paste0(
  prev_block_hash = character(),
  block_data = character(),  # Merkle root hash
  block_number = integer(),
  nonce = integer()
)
```

And then mine the block to find its hash. Once a block hash has been found, it becomes the input to the next block. The example below creates a blockchain with three blocks containing nothing but an empty string as `block_data` field. The first block's hash is deliberately left empty in this example.

```{r block-chain-code, eval=TRUE, include = TRUE}
n_blocks <- 3
max_nonce <- 1e6
difficulty <- 2
block_chain <- list("")
for(block_i in 2:(n_blocks)) {
  this_block <- paste0(
    prev_block_hash = block_chain[[block_i - 1]],
    block_data = "",
    block_number = block_i,
    nonce = 0
  )
  for(i in 1:max_nonce) {
    hash_i <- openssl::sha256(
      paste0(
        prev_block_hash = block_chain[[block_i - 1]],
        block_data = "",
        block_number = block_i,
        nonce = i
      )
    )
    if(substr(hash_i, 1, difficulty) == strrep(0, difficulty)) {
      break
    }
  }
  block_chain[[block_i]] <- hash_i
}
names(block_chain) <- paste0("block_", 1:n_blocks)
```

```{r, eval=FALSE, include=FALSE}
make_block_chain <- function(prev_block_hash, difficulty=2,
                             max_nonce=1e6, n_blocks = 1) {
  block_chain <- list(prev_block_hash)
  for(block_i in 2:(n_blocks  + 1)) {
    this_block <- paste0(
      prev_block_hash = block_chain[[block_i - 1]],
      block_data = "",
      block_number = block_i,
      nonce = 0
    )
    for(i in 1:max_nonce) {
      hash_i <- openssl::sha256(
        paste0(
          prev_block_hash = block_chain[[block_i - 1]],
          block_data = "",
          block_number = block_i,
          nonce = i
        )
      )
      if(substr(hash_i, 1, difficulty) == strrep(0, difficulty)) {
        break
      }
    }
    block_chain[[block_i]] <- hash_i
  }

  return(block_chain)
}

options(microbenchmark.unit="ms")
chains_1 <- microbenchmark::microbenchmark(times = 30,
  "One Block, Level 1" = make_block_chain(difficulty = 1, n_blocks = 1),
  "One Block, Level 2" = make_block_chain(difficulty = 2, n_blocks = 1),
  "One Block, Level 3" = make_block_chain(difficulty = 3, n_blocks = 1)
)
chains_2 <- microbenchmark::microbenchmark(times = 30,
  "Two Blocks, Level 1" = make_block_chain(difficulty = 1, n_blocks = 2),
  "Two Blocks, Level 2" = make_block_chain(difficulty = 2, n_blocks = 2),
  "Two Blocks, Level 3" = make_block_chain(difficulty = 3, n_blocks = 2)
)
chains_3 <- microbenchmark::microbenchmark(times = 30,
  "Three Blocks, Level 1" = make_block_chain(difficulty = 1, n_blocks = 3),
  "Three Blocks, Level 2" = make_block_chain(difficulty = 2, n_blocks = 3),
  "Three Blocks, Level 3" = make_block_chain(difficulty = 3, n_blocks = 3)
)
chains_4 <- microbenchmark::microbenchmark(times = 30,
  "Four Blocks, Level 1" = make_block_chain(difficulty = 1, n_blocks = 4),
  "Four Blocks, Level 2" = make_block_chain(difficulty = 2, n_blocks = 4),
  "Four Blocks, Level 3" = make_block_chain(difficulty = 3, n_blocks = 4)
)
chain_diffulty <- microbenchmark::microbenchmark(times = 30,
  "Four Blocks, Level 1" = make_block_chain(difficulty = 1, n_blocks = 4),
  "Four Blocks, Level 2" = make_block_chain(difficulty = 2, n_blocks = 4),
  "Four Blocks, Level 3" = make_block_chain(difficulty = 3, n_blocks = 4),
  "Four Blocks, Level 4" = make_block_chain(difficulty = 4, n_blocks = 4)
)
save(chains_1, chains_2, chains_3,chains_4,chain_difficulty, file = "data/blockchains.rda")
```

```{r block-chain-list, echo = FALSE}
fig_caption <- paste("A sample blockchain with", length(block_chain), "blocks")
block_chain_df <- data.frame(Hash = as.character(block_chain))
rownames(block_chain_df) <- paste("Block", 1:nrow(block_chain_df))
knitr::kable(block_chain_df, caption=fig_caption)
```

```{r block-chain-times, echo = FALSE}
if(!interactive()) {
  load(file = "data/blockchains.rda")
}

fig_caption <- paste("Time (in milliseconds) required to mine increasingly difficult chains")
times_df <- as.data.frame(summary(rbind(chains_1, chains_2, chains_2, chains_4)))
times_df <- times_df[, c("expr", "min", "median", "max")]
knitr::kable(times_df, caption=fig_caption)
```

```{r block-chain-plot, fig.cap="Time vs. difficulty, 4 blocks.", echo = FALSE}
boxplot(chain_difficulty, ylab = "Log(time)", xlab = "Difficulty", main = "Chain of 4 blocks")
```

Table \@ref(tab:block-chain-times) shows benchmark times obtained using the `microbenchmkark` package [@R-microbenchmark] while mining blockchains of increasing difficulty (Level) and length (Blocks). We can see in Figure \@ref(fig:block-chain-plot) that the time needed to mine 4 blocks increases exponentially with the difficulty, which is controlled with the number of leading zeros required by the hash to be considered valid.

We can also calculate the probability of successfully compromising the network as a function of an attacker's probability of finding the next block in the chain vs. an honest node finding it. We can compute the probability $q_z$ that an attacker will catch up from $z$ blocks behind following @nakamoto-2008 as follows:

\begin{align}
q_z = 1 - \sum_{k=0}^{z} \frac{\lambda^k e^{-\lambda}}{k!} \left\{ 1 - \left(\frac{q}{p}\right)^{(z-k)}\right\} (\#eq:attacker-prob)
\end{align}

Where,

$$
\begin{aligned}
&p = \text{probability an honest node finds the next block.}\\
&q = \text{probability the attacker finds the next block.}\\
&q_z = \text{probability the attacker will ever catch up from $z$ blocks behind.}
\end{aligned}
$$

Which can be translated into the following R code: ^[This is nothing but an R version of the original C code in the paper by @nakamoto-2008.]

```{r}
attacker_success_probability <- function(q_prob, z_blocks) {
  p_prob <- 1 - q_prob
  lambda <- z_blocks * (q_prob / p_prob)
  sum_prob <- 1
  for(k in 0:z_blocks) {
    poisson_prob <- exp(-lambda)
    i <- 1
    while(i <= k) {
      poisson_prob <- poisson_prob * lambda / i
      i <- i + 1
    }
    sum_prob <- sum_prob - poisson_prob * (1 - (q_prob / p_prob)^(z_blocks - k))
  }
  return(sum_prob)
}
```

The setup used in Figure \@ref(fig:block-chain-plot) corresponds to `z_blocks = 4`, and the attacker's success probability can be found through Equation \@ref(eq:attacker-prob) as summarized in Table \@ref(tab:attacker-prob) .

```{r attacker-prob, echo=FALSE}
q_probs_0 <- seq(0, 0.5, 0.05)
blocks_behind <- seq.int(0, 50, length.out = 11)
attacker_table <- data.frame(row.names = blocks_behind)

for (p in q_probs_0) {
  attacker_probs <- as.data.frame(format(sapply(blocks_behind, attacker_success_probability, q_prob = p), nsmall=4))
  colnames(attacker_probs) <- paste("q =", p)
  rownames(attacker_probs) <- blocks_behind

  attacker_table <<- cbind(attacker_table, attacker_probs)
}

fig_caption <- paste("The probability of success drops exponentially with z.")
knitr::kable(attacker_table[, c(2,3,5,8)], caption=fig_caption)
```

```{r attacker-prob-plot, dev="pdf", fig.cap="Attacker's success drops exponentially with the number of blocks", warning=FALSE, echo=FALSE}

fig <- plotly::plot_ly(
    x = blocks_behind,
    y = as.numeric(attacker_table[, 1]),
    name = colnames(attacker_table)[1], type = "scatter", mode = "markers"
  ) %>%
  layout(
    title="Attacker's success probability vs. # of blocks behind",
    xaxis = list(title="# of Blocks"),
    yaxis = list(title="Probability")
  )

for (i in 2:ncol(attacker_table)) {
  fig <- fig %>% add_trace(
    x = blocks_behind,
    y = as.numeric(attacker_table[, i]),
    name = colnames(attacker_table)[i]
  )
}

fig
```

`r if (knitr::is_latex_output()) '# Market value' else  '## Market value'`

In this section we have introduced the mechanics behind the Blockchain technology without mentioning prices nor quantiles so far. Several applications based on the Blockchain technology are currently in development/production [@blockchain-2021] as evidence of Blockchain's *technological* or *intrinsic* value. However they do not necessarily have a relationship with Bitcoin nor with its price. Therefore, we might wonder whether there is *any* value in a cryptocurrency at all.

Linking computer resources to a *token* (the cryptocurrency) through a proof-of-work algorithm consumes electricity, which is mainly produced from coal and gas with only 28% of global consumption coming from renewable sources [@iea-stats]. Growing investors' concerns about the environmental impact of Bitcoin mining is one of the reasons behind alternative mining algorithms since a financial transaction's environmental impact is directly linked to the computing power required to process such transactions through the network. As a consequence of such innovations and with over `6000` cryptocurrencies available worldwide [@statista-stats], investors' capital allocation to this new asset class is increasingly diversifying out of the original Blockchain implementation, i.e., Bitcoin.

We commonly use an asset's closing price as a proxy of the asset's (market) value, and Bitcoin's market price remains far from zero since its inception despite the legal and technological challenges [@bitcoin-china]. The goal of the following section is to test different MCMC methods and $\var$ models to forecast quantiles of the distribution of returns of a cryptocurrency such as Bitcoin. For this, we will apply standard econometric methods to a vector of cryptocurrency returns as we aim to get a forecast of $\var$, which represents a quantile of the distribution of financial returns.



