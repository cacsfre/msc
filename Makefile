bs4:
	Rscript -e 'bookdown::render_book("index.Rmd", "bookdown::bs4_book")'

gitbook:
	Rscript -e 'bookdown::render_book("index.Rmd", "bookdown::gitbook")'

pdf:
	rm _main.*
	Rscript -e 'bookdown::render_book("index.Rmd", "bookdown::pdf_book")'

run:
	Rscript -e 'bookdown::serve_book()'

depends:
	sudo apt-get update && sudo apt-get install -y \
	libcurl4-openssl-dev \
	libssl-dev \
	libxml2-dev
	Rscript -e "install.packages(c('remotes', 'openssl', 'GAS', 'jsonlite', 'plotly', 'websocket', 'coda', 'MASS', 'DiagrammeR', 'microbenchmark', 'bookdown', 'downlit', 'rmarkdown', 'adaptMCMC', 'GA', 'pso', 'atmcmc', 'fmcmc', 'MSGARCH'))" \
	Rscript -e "remotes::install_gitlab('cacsfre/caviarma')" \

ulaval:
	mkdir ulaval
	cp _book/_main.tex ulaval
	cp _book/_main.pdf ulaval
	cp -r _bookdown_files/*_files ulaval
	cp book.bib ulaval
	cp packages.bib ulaval
	zip -r memoire.zip ulaval
