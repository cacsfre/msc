# Forecasting quantiles of cryptocurrency returns using MCMC algorithms

This is the code and text behind my master's thesis [Forecasting quantiles of cryptocurrency returns using MCMC algorithms](https://cacsfre.gitlab.io/msc).  The site is built with [bookdown](https://bookdown.org/yihui/bookdown/).

To zip files for ULaval:

```
mkdir ulaval
cp _book/_main.tex ulaval
cp _book/_main.pdf ulaval
cp -r _bookdown_files/*_files ulaval
cp book.bib ulaval
cp packages.bib ulaval
zip -r memoire.zip ulaval
```

